<?php
namespace Drupal\ghost_dev;
use Drupal\ghost\Core\Variable\VariableController\VariableController;
use Drupal\ghost\Traits\InitialiserTrait;

/**
 * Class Dev.
 *
 * @package Drupal\ghost_dev
 */
class Dev {

  use InitialiserTrait;

  /**
   * Constant to define the Dev mode.
   */
  const DEV_MODE_VAR = 'ghost_dev_mode';

  /**
   * A VariableController.
   *
   * @var VariableController
   */
  protected $variableController;

  /**
   * Dev constructor.
   */
  public function __construct() {

    $this->variableController = VariableController::create();
  }

  /**
   * Get the current developer mode.
   *
   * @return bool
   *   TRUE if in dev mode.
   */
  public function getDevMode() {

    return $this->variableController->get(self::DEV_MODE_VAR, FALSE);
  }

  /**
   * Set the dev mode.
   *
   * @param bool $mode
   *   A dev mode flag.
   */
  public function setDevMode($mode) {

    $mode = (bool) $mode;

    $this->variableController->set(self::DEV_MODE_VAR, $mode);
  }

}
