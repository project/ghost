<?php
namespace Drupal\ghost\Logger;

/**
 * Class VarLogger.
 *
 * @package Drupal\ghost\Logger
 */
class VarLogger extends Logger {

  /**
   * Constructor.
   *
   * @param string $caller
   *   The module calling the Log function.
   * @param string $message
   *   The message to log.
   * @param array $vars
   *   (Optional) Variables to substitute into the message.
   * @param int $severity
   *   (Optional) A Watchdog severity level. Defaults to WATCHDOG_NOTICE.
   * @param bool $message_area
   *   (Optional) Whether to log to drupal_set_message as well. Defaults to
   *   FALSE.
   *
   * @return VarLogger
   *   This Logger
   */
  static public function init($caller, $message = NULL, $vars = array(), $severity = WATCHDOG_NOTICE, $message_area = FALSE) {
    return parent::init($caller, $message, $vars, $severity, $message_area);
  }

  /**
   * Prints a variable to the 'message' area of the page.
   *
   * @param mixed $input
   *   An arbitrary value to output.
   * @param string $name
   *   Optional name for identifying the output.
   *
   * @return $this
   *   An instance of VarLogger
   */
  public function logVars($input, $name = NULL) {
    $export = $this->kprintr($input, TRUE, $name);

    $this->setMessage($export);

    return $this;
  }

  /**
   * Krumo print.
   */
  public function kprintr($input, $return = FALSE, $name = NULL) {
    if (!module_exists('devel')) {
      drupal_set_message('Ghost var logging requires the devel module to be enabled.');
    }

    has_krumo();

    return $return ? (isset($name) ? $name . ' => ' : '') . krumo_ob($input) : krumo($input);
  }

}
